const sumAll = function(a, b) {
  if (!Number.isInteger(a) || !Number.isInteger(b)) return 'ERROR';
  if ((a < 0) || (b < 0)) return 'ERROR';
  let sum = 0;
  if (a > b) {
    let temp = a;
    a = b;
    b = temp;
  }
  for (i = a; i <= b; i++) {
    sum += i;
  }
  
  return sum;
}

module.exports = sumAll
