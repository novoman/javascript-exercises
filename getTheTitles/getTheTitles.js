const getTheTitles = function(arr) {
	let result_arr = [];
	arr.forEach(obj => result_arr.push(obj['title']));

	return result_arr;
}

module.exports = getTheTitles;
