function add (a, b) {
	return a + b;
}

function subtract (a, b) {
	return a - b;
}

function sum (arr) {
	let sum = 0;
	arr.forEach(function(el) {
		sum += el;
	});

	return sum;
}

function multiply (arr) {
	let mult = 1;
	arr.forEach(function(el) {
		mult *= el;
	});

	return mult;
}

function power(x, pow) {
	return x ** pow;
}

function factorial(x) {
	if (x == 0) return 1;

	let fact = 1;
	for (let i = 1; i <= x; i++) {
		fact *= i;
	}

	return fact;
}

module.exports = {
	add,
	subtract,
	sum,
	multiply,
    power,
	factorial
}