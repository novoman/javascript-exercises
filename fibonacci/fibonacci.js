const fibonacci = function(number) {
	number = parseInt(number);
	if (number < 1) return "OOPS";
	if (number == 1 || number == 2) { return 1 }
	else return fibonacci(number - 1) + fibonacci(number - 2);	
}

module.exports = fibonacci
