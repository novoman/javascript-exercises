let findTheOldest = function(people) {
	let oldest_man = { name: '', years: 0 };
	people.forEach(function(man) {
		let man_age = (man['yearOfDeath'] ? man['yearOfDeath'] : (new Date().getFullYear())) - man['yearOfBirth'];
		if (man_age > oldest_man['years']) {
			oldest_man['name'] = man['name'];
			oldest_man['years'] = man_age;
		}
	});

	return oldest_man;
}

module.exports = findTheOldest
