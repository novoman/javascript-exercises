const caesar = function(text, shift) {
	const ALPHABET_ARRAY = 'abcdefghijklmnopqrstuvwxyz'.split('');
	const ALPHABET_ARRAY_LENGTH = ALPHABET_ARRAY.length;

	if (shift > ALPHABET_ARRAY_LENGTH) { shift %= ALPHABET_ARRAY_LENGTH; }
	let result_str = '';
	let text_array = text.split('');
	text_array.forEach(function(letter) {
		if (/^[a-zA-Z]+$/.test(letter)) {
			let uppercase_flag = false;

			if (letter == letter.toUpperCase()) { uppercase_flag = true; }

			letter = letter.toLowerCase();
			let result_letter = '';
			let el_index = ALPHABET_ARRAY.findIndex(function(el) { return el == letter });
			if (ALPHABET_ARRAY_LENGTH <= el_index + shift) {	result_letter = ALPHABET_ARRAY[(el_index + shift) - ALPHABET_ARRAY_LENGTH];	}
			else if (el_index + shift <= 0) { result_letter = ALPHABET_ARRAY[ALPHABET_ARRAY_LENGTH + (el_index + shift)]; }
			else { result_letter = ALPHABET_ARRAY[el_index + shift]; }

			if (uppercase_flag) { result_str += result_letter.toUpperCase(); }
			else { result_str += result_letter; }
		}
		else {
			result_str += letter;
		}
	});

	return  result_str;
}

module.exports = caesar
