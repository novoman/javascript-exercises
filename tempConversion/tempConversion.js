const ftoc = function(x) {
  return Math.round((x - 32) * 5 / 9 * 10) / 10 ;
}

const ctof = function(x) {
  return Math.round(x * 9 / 5 * 10) / 10 + 32;
}

module.exports = {
  ftoc,
  ctof
}
