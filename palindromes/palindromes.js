const palindromes = function(str) {
	str = str.replace(/[^a-zA-Z]/g, '');
	str = str.toLowerCase()
	let arr = str.split('');
	arr = arr.reverse();
	let rev_str = arr.join('');

	return str == rev_str; 
}

module.exports = palindromes
